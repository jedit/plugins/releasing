#!/bin/bash

# this works because the container user has the same uid as my desktop user

if [ -n "$WAYLAND_DISPLAY" ]; then
# Uses x11docker to have a nested x11 display accessible in the container
# to test jEdit.
# Remember to install openbox to have a window manager
exec x11docker --backend=docker --init=none --no-setup -I -i --user=RETAIN \
	   -- \
	   --volume=$(pwd):/releasing \
	   --volume=$(pwd)/sandbox/ivy:/home/jedit/.ivy2:rw \
	   -- \
	   "$@"
else
# uses xauth and pass the display socket inside the container.
XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth
touch $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

sudo docker run --rm -it --name jedit \
        --volume=$XSOCK:$XSOCK:rw \
        --volume=$XAUTH:$XAUTH:rw \
        --env="XAUTHORITY=${XAUTH}" \
        --env="DISPLAY" \
        --user="jedit" \
        -v $(pwd):/releasing \
        --volume=$(pwd)/sandbox/ivy:/home/jedit/.ivy2:rw \
        "$@"
fi
