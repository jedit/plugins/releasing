# _releasing

tools for releasing jEdit plugins.

# Docker images

A new docker container from an identified image is used to ensure a pristine build and test environment.

The environment is based on ubuntu lts 18.04. It contains:
 - a single openjdk jdk version (1.8 for jdk8, 11 for jdk11, etc.)
 - build deps (ant, junit, etc.)
 - stable jEdit releases installed (only 5.5.0 at the moment)
 - pre-fetched ivy 2.5.0

# Install

Get sources

	git clone https://gitlab.com/jedit/plugins/_releasing.git
	cd _releasing
	git svn clone -r24991 svn://svn.code.sf.net/p/jedit/svn/build-support/trunk build-support
	git svn clone -r24991 svn://svn.code.sf.net/p/jedit/svn/pjo/trunk build-support

Edit build.properties for usage in docker

	sandbox.dir=/releasing/sandbox
	jedit.version.05.05.install.dir=/jedit/5.5.0
	jedit.version.05.05.settings.dir=/home/jedit/.jedit
	build.support=/releasing/build-support
	junit.jar=/usr/share/java/junit4.jar

# Running

For instance, to release Ancestors 1.2.0:

	./docker-package.sh registry.gitlab.com/jedit/plugins/releasing/jdk11:master
		ant download-git build view package \
		-Dplugin.name=Ancestor -Dplugin.version=1.2.0 -Dplugin.tag=1.2.0 \
		-Dclone.url=git://git.code.sf.net/p/jedit/Ancestor\
		-Dcompiler.source=1.11 -Dcompiler.target=1.11

# Upload and update plugin list

As before, see pjo/doc/pjo.txt
