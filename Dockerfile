ARG UBUNTU_VERSION=18.04
FROM ubuntu:${UBUNTU_VERSION}

ARG JDK_VERSION=8

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y -q \
        xvfb sudo \
         git subversion wget \
         ant junit4 \
         xsltproc \
         pandoc python3

RUN if [ "$JDK_VERSION" != 15 ]; then \
     DEBIAN_FRONTEND=noninteractive apt-get install -y -q openjdk-${JDK_VERSION}-jdk; \
     fi

RUN if [ "$JDK_VERSION" != 11 ]; then \
     DEBIAN_FRONTEND=noninteractive apt-get purge -y -q openjdk-11-jre-headless; \
     fi

# openjdk-15 disappeared from ubuntu 22 repos
RUN if [ "$JDK_VERSION" = 15 ]; then \
	cd /opt; \
    DEBIAN_FRONTEND=noninteractive apt-get install -y -q $(apt-cache depends openjdk-17-jre | awk '/Depends:/ && !/Depends: openjdk/ {print $2}') && \
	wget --no-verbose https://github.com/AdoptOpenJDK/openjdk15-binaries/releases/download/jdk-15.0.2%2B7/OpenJDK15U-jdk_x64_linux_hotspot_15.0.2_7.tar.gz \
	&& tar xf OpenJDK15U-jdk_x64_linux_hotspot_15.0.2_7.tar.gz \
	&& rm OpenJDK15U-jdk_x64_linux_hotspot_15.0.2_7.tar.gz \
	&& for cmd in jdk-15.0.2+7/bin/*; do ln -s $(realpath $cmd) /usr/bin/;done \
	&& echo JAVA_HOME=/opt/jdk-15.0.2+7 > /etc/profile.d/99-openjdk15.sh; \
	fi

# could build package but not test using headless and no xvfb

# install gnupg2
# RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E6A233DBE3AFBEFC
# COPY sources-jedit /etc/apt/sources.list.d/jedit
# RUN apt-get update \
#  && DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y -q \

WORKDIR /tmp

RUN wget --no-verbose "https://freefr.dl.sourceforge.net/project/jedit/jedit/5.2.0/jedit5.2.0install.jar" \
 && java -jar jedit5.2.0install.jar auto /jedit/5.2.0 unix-script=/usr/bin \
 && rm jedit5.2.0install.jar \
 && mv /usr/bin/jedit /usr/bin/jedit520

RUN wget --no-verbose "https://freefr.dl.sourceforge.net/project/jedit/jedit/5.3.0/jedit5.3.0install.jar" \
 && java -jar jedit5.3.0install.jar auto /jedit/5.3.0 unix-script=/usr/bin \
 && rm jedit5.3.0install.jar \
 && mv /usr/bin/jedit /usr/bin/jedit530

RUN wget --no-verbose "https://freefr.dl.sourceforge.net/project/jedit/jedit/5.4.0/jedit5.4.0install.jar" \
 && java -jar jedit5.4.0install.jar auto /jedit/5.4.0 unix-script=/usr/bin \
 && rm jedit5.4.0install.jar \
 && mv /usr/bin/jedit /usr/bin/jedit540

RUN wget --no-verbose "https://freefr.dl.sourceforge.net/project/jedit/jedit/5.5.0/jedit5.5.0install.jar" \
 && java -jar jedit5.5.0install.jar auto /jedit/5.5.0 unix-script=/usr/bin \
 && rm jedit5.5.0install.jar \
 && mv /usr/bin/jedit /usr/bin/jedit550

RUN if [ "$JDK_VERSION" -ge 11 ]; then \
 wget --no-verbose "https://freefr.dl.sourceforge.net/project/jedit/jedit/5.6.0/jedit5.6.0install.jar" \
 && java -jar jedit5.6.0install.jar auto /jedit/5.6.0 unix-script=/usr/bin \
 && rm jedit5.6.0install.jar; \
 fi

RUN if [ "$JDK_VERSION" -ge 11 ]; then \
 wget --no-verbose "https://freefr.dl.sourceforge.net/project/jedit/jedit/5.7.0/jedit5.7.0install.jar" \
 && java -jar jedit5.7.0install.jar auto /jedit/5.7.0 unix-script=/usr/bin \
 && rm jedit5.7.0install.jar; \
 fi

WORKDIR /releasing/pjo/ant

ENV USERNAME=jedit
RUN useradd -m $USERNAME && \
        echo "$USERNAME:$USERNAME" | chpasswd && \
        usermod --shell /bin/bash $USERNAME && \
        usermod -aG sudo $USERNAME && \
        echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/$USERNAME && \
        chmod 0440 /etc/sudoers.d/$USERNAME && \
        # Replace 1000 with your user/group id
        usermod  --uid 1000 $USERNAME && \
        groupmod --gid 1000 $USERNAME

USER jedit

ENV LANG=C.UTF-8

# COPY pjo/ant /releasing/pjo/ant
# COPY build-support /releasing/build-support

# RUN cd /releasing/pjo/ant \
#  && ant cache-lib-scripting
